import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * saves an image file contained in request
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 4,
        maxRequestSize = 1024 * 1024 * 8
)
public class ImageUploadController extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUploadController.class);
    private static final String TYPES_PARAM_NAME = "MIME-types";
    private static final String TYPES_SEPARATOR = ", ";
    private static final String INDEX_PAGE = "index.jsp";
    private static final File DEST_DIR = new File("uploads\\");
    private static List<String> suitableTypes;

    @Override
    public void init() {
        String[] types = getInitParameter(TYPES_PARAM_NAME).split(TYPES_SEPARATOR);
        suitableTypes = Arrays.asList(types);
        System.out.println(suitableTypes);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException, ServletException {
        Part img = req.getPart("image");
        if (img == null || !isTypeValid(img)) {
            forwardError(req, resp, "Wrong image type");
            return;
        }

        if (!DEST_DIR.exists() && !DEST_DIR.mkdirs()) {
            LOGGER.error("Failed to create directory: " + DEST_DIR);
            forwardError(req, resp, "Unable to save the file");
            return;
        }

        try {
            long id = Long.parseLong(req.getParameter("id"));
            String destFile = getDestFile(img, id);

            img.write(destFile);

            String info = destFile + " uploaded";
            LOGGER.info(info);
            req.setAttribute("info", info);
            RequestDispatcher rd = req.getRequestDispatcher(INDEX_PAGE);
            rd.forward(req, resp);
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            forwardError(req, resp, "Book not found :(");
        }
    }

    private String getDestFile(final Part part, final long id) {
        String fileName = part.getSubmittedFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));
        return DEST_DIR.getAbsolutePath() + "\\cover" + id + extension;
    }

    private boolean isTypeValid(final Part part) {
        String type = part.getContentType();
        return suitableTypes.contains(type);
    }

    private void forwardError(final HttpServletRequest req, final HttpServletResponse resp, final String message)
            throws ServletException, IOException {
        req.setAttribute("errorMessage", message);
        RequestDispatcher rd = req.getRequestDispatcher(INDEX_PAGE);
        rd.forward(req, resp);
    }
}
