package filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Logs requests to mapped paths
 */
@WebFilter("/uploadImage")
public class LoggingFilter extends HttpFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    public void doFilter(final HttpServletRequest req,
                         final HttpServletResponse resp,
                         final FilterChain chain)
    throws ServletException, IOException {
        LOGGER.info(req.getRemoteAddr() + " request to " + req.getRequestURI());
        chain.doFilter(req, resp);
    }
}
