<%@ page contentType="text/html;charset=UTF-8"%>
<html>
  <head>
    <title>Upload Cover</title>
  </head>
  <body>
      <form action="/uploadImage" method="post" enctype="multipart/form-data">
          <input type="file" name="image">
          <input type="hidden" name="id" value="${param.id}">
          <input type="submit" value="upload">
      </form>
  </body>
</html>
