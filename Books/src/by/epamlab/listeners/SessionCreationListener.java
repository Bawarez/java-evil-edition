package by.epamlab.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Logs session creation and destruction.
 * Also adds SessionActivationListener that logs session activation and deactivation.
 */
@WebListener
public class SessionCreationListener implements HttpSessionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionCreationListener.class);

    @Override
    public void sessionCreated(final HttpSessionEvent event) {
        HttpSession session = event.getSession();
        session.setAttribute("LoggingListener", new LoggingSessionListener());
        LOGGER.info("Session " + session.getId() + " was created at " + session.getCreationTime());
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent event) {
        HttpSession session = event.getSession();
        LOGGER.info("Session " + session.getId() + " was destroyed at " + System.currentTimeMillis());
    }
}
