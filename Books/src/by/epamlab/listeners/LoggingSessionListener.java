package by.epamlab.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

/**
 * Logs session activation and deactivation.
 */
@WebListener
public class LoggingSessionListener implements HttpSessionActivationListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingSessionListener.class);

    @Override
    public void sessionDidActivate(final HttpSessionEvent event) {
        HttpSession session = event.getSession();
        LOGGER.info("Session " + session.getId() + " was activated at " + System.currentTimeMillis());
    }

    @Override
    public void sessionWillPassivate(final HttpSessionEvent event) {
        HttpSession session = event.getSession();
        LOGGER.info("Session " + session.getId() + " was deactivated at " + System.currentTimeMillis());
    }
}
