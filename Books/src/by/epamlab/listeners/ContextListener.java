package by.epamlab.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        String contextName = context.getServletContextName();

        LOGGER.info("Application name: " + contextName);
        context.getServletRegistrations()
                .values()
                .forEach(s -> LOGGER.info("Servlet " + s.getName() + " is mapped to " + s.getMappings()));
    }
}
