package by.epamlab.service;

import by.epamlab.exceptions.SourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionPool.class.getName());
    private static final String POOL_NAME = "java:comp/env/jdbc/connectionPool";
    private static volatile ConnectionPool instance = null;

    private ConnectionPool() { }

    public static ConnectionPool getInstance() {
        if (instance == null) {
            synchronized (ConnectionPool.class) {
                if (instance == null) {
                    instance = new ConnectionPool();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        try {
            Context context = new InitialContext();
            DataSource ds = (DataSource) context.lookup(POOL_NAME);
            return ds.getConnection();
        } catch (NamingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SourceException(e.getMessage());
        }
    }
}