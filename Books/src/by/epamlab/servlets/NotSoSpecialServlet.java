package by.epamlab.servlets;

import by.epamlab.Const;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Here must be a Javadoc comment, but I forgot to write it
 */
@WebServlet(urlPatterns = "/anotherServlet")
public class NotSoSpecialServlet extends HttpServlet {
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            Thread.sleep(1000);
            req.setAttribute("secondAttribute", "second attribute has been calculated!");
            req.setAttribute(Const.ATTR_PAGE, "someSpecialPage.jsp");
            req.getRequestDispatcher(Const.PAGE_INDEX)
                    .forward(req, resp);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
