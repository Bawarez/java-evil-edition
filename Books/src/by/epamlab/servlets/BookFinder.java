package by.epamlab.servlets;

import by.epamlab.Const;
import by.epamlab.beans.Book;
import by.epamlab.dao.BookDAO;
import by.epamlab.dao.SqlBookDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Gets a book entity by its id and returns it to client
 */
@WebServlet(urlPatterns = "/showBook", name = "Single book retriever")
public class BookFinder extends HttpServlet {
    private BookDAO bookDAO = SqlBookDAO.getInstance();

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
       try {
            Long id = Long.parseLong(req.getParameter(Book.ID));
            Book book = bookDAO.getBook(id);

            req.setAttribute(Const.ATTR_BOOK, book);
            req.setAttribute(Const.ATTR_EDIT_BOOK, true);
        } catch (NumberFormatException e) {
            req.setAttribute(Const.ATTR_ERROR_MSG, "The book does not exist");
        }

        RequestDispatcher rd = req.getRequestDispatcher(Const.PATH_SHOW);
        rd.include(req, resp);
    }
}
