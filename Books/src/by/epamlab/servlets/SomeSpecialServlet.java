package by.epamlab.servlets;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Here can be your Javadoc comment.
 */
@WebServlet(urlPatterns = "/someSpecialPage", asyncSupported = true)
public class SomeSpecialServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) {
        AsyncContext asyncContext = req.startAsync();
        asyncContext.setTimeout(9410);

        asyncContext.start(() -> {
            try {
                Thread.sleep(1500);
                asyncContext.getRequest().setAttribute("firstAttribute", "first value has been calculated!");
                asyncContext.dispatch("/anotherServlet");
            } catch (InterruptedException e) {
               throw new RuntimeException(e.getMessage(), e);
            }
        });
    }
}
