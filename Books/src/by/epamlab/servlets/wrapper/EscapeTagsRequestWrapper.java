package by.epamlab.servlets.wrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Replaces characters '>', '<', '/' in request parameters and headers with their codes
 */
public class EscapeTagsRequestWrapper extends HttpServletRequestWrapper {
    private static final String[][] ENTITIES = {{"<", "&#60;"},
                                                {">", "&#62"},
                                                {"/", "&#8260"}};
    /**
     * @param req
     *        wrapped request
     */
    public EscapeTagsRequestWrapper(final HttpServletRequest req) {
        super(req);
    }

    @Override
    public String getParameter(final String name) {
        return escapeTags(super.getParameter(name));
    }

    @Override
    public String[] getParameterValues(final String name) {
        String[] values = super.getParameterValues(name);
        if (values == null) {
            return null;
        }
        for (int i = 0; i < values.length; i++) {
            values[i] = escapeTags(values[i]);
        }
        return values;
    }

    @Override
    public String getHeader(final String name) {
        return (escapeTags(super.getHeader(name)));
    }

    @Override
    public Enumeration<String> getHeaders(final String name) {
        Enumeration<String> headers = super.getHeaders(name);
        ArrayList<String> buffer = new ArrayList<>();

        while (headers.hasMoreElements()) {
            String next = headers.nextElement();
            buffer.add(escapeTags(next));

        }

        return Collections.enumeration(buffer);
    }

    private String escapeTags(final String value) {
        if (value == null) {
            return null;
        }
        String result = value;
        for (String[] entityPair : ENTITIES) {
            result = result.replaceAll(entityPair[0], entityPair[1]);
        }
        return result;
    }
}
