package by.epamlab.servlets;

import javax.servlet.UnavailableException;

class ServiceDisabler {
    private static final int DEFAULT_DISABLE_TIME = 8;
    private static final String MESSAGE = "Service is currently unavailable.";

    static void disableService(String timeStr) throws UnavailableException {
        int disableTime;

        try {
            disableTime = Integer.parseInt(timeStr);
        } catch (NumberFormatException e) {
            disableTime = DEFAULT_DISABLE_TIME;
        }

        throw new UnavailableException(MESSAGE, disableTime);
    }
}
