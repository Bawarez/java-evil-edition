package by.epamlab.servlets;


import by.epamlab.Const;
import by.epamlab.dao.BookDAO;
import by.epamlab.dao.SqlBookDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/bookManagement")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = "user_management")
)
public class BookManagementController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("management", true);
        req.getRequestDispatcher(Const.PATH_SHOW).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            Long id = Long.parseLong(req.getParameter("id"));
            BookDAO bookDAO = SqlBookDAO.getInstance();
            bookDAO.deleteBook(id);

            resp.sendRedirect("/bookManagement");
        } catch (NumberFormatException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Failed to receive book id");
        }
    }
}
