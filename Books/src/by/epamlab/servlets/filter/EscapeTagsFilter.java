package by.epamlab.servlets.filter;

import by.epamlab.Const;
import by.epamlab.servlets.wrapper.EscapeTagsRequestWrapper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Wraps requests into EscapeTagsRequestWrapper
 */
@WebFilter(urlPatterns = {Const.PATH_ADD, Const.PATH_UPDATE})
public class EscapeTagsFilter extends HttpFilter {

    @Override
    public void doFilter(final HttpServletRequest req,
                         final HttpServletResponse resp,
                         final FilterChain chain)
                        throws IOException, ServletException {
        EscapeTagsRequestWrapper reqWrapper = new EscapeTagsRequestWrapper(req);
        chain.doFilter(reqWrapper, resp);
    }
}
