package by.epamlab.servlets;

import by.epamlab.Const;
import by.epamlab.beans.Book;
import by.epamlab.dao.BookDAO;
import by.epamlab.dao.SqlBookDAO;
import by.epamlab.factories.BookFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Extracts information about book from request and adds or updates appropriate entity in a storage
 */
@WebServlet(urlPatterns = {Const.PATH_ADD, Const.PATH_UPDATE}, name = "Book base updater")
public class UpdateBookController extends HttpServlet {
    private BookDAO bookDAO = SqlBookDAO.getInstance();

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
        throws ServletException, IOException {
        Book book = BookFactory.createBook(req);
        if (bookDAO.saveBook(book)) {
            resp.sendRedirect(Const.PATH_SHOW);
        } else {
            req.setAttribute(Const.ATTR_BOOK, BookFactory.createBook(req));
            req.setAttribute(Const.ATTR_ERROR_MSG, "Failed to save the book");
            req.setAttribute(Const.ATTR_EDIT_BOOK, true);

            RequestDispatcher rd = req.getRequestDispatcher(Const.PAGE_INDEX);
            rd.forward(req, resp);
        }
    }
}
