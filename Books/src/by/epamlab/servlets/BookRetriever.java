package by.epamlab.servlets;

import by.epamlab.Const;
import by.epamlab.beans.Book;
import by.epamlab.dao.BookDAO;
import by.epamlab.dao.SqlBookDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@WebServlet(urlPatterns = Const.PATH_SHOW, name = "Book viewer")
public class BookRetriever extends HttpServlet {
    private static final String PARAM_DISABLE = "disable";
    private static final String PARAM_SWITCH_ACCESSIBILITY_TO_DISABLE = "switchAccessibilityToDisable";
    private static final String ATTR_BOOK_LIST = "bookList";
    private static volatile AtomicBoolean canBeDisabled = new AtomicBoolean(true);
    private static BookDAO bookDAO = SqlBookDAO.getInstance();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         configureAvailability(req);
         configureAccessibilityToDisable(req);

         List<Book> books = bookDAO.getBooks();
         req.setAttribute(ATTR_BOOK_LIST, books);

         req.setAttribute(Const.ATTR_PAGE, Const.PAGE_BOOKS);
         RequestDispatcher rd = req.getRequestDispatcher(Const.PAGE_INDEX);
         rd.forward(req,resp);
    }

    private void configureAvailability(HttpServletRequest req) throws UnavailableException {
        if (canBeDisabled.get()) {
            String disableTime = req.getParameter(PARAM_DISABLE);
            if (disableTime != null) {
                ServiceDisabler.disableService(disableTime);
            }
        }
    }

    private void configureAccessibilityToDisable(HttpServletRequest req) {
        String switchAccessibilityToDisable = req.getParameter(PARAM_SWITCH_ACCESSIBILITY_TO_DISABLE);
        if ("true".equals(switchAccessibilityToDisable)) {
            canBeDisabled.set(!canBeDisabled.get());
        }
    }
}
