package by.epamlab;

/**
 * Contains necessary constants using by the application
 */
public final class Const {
    private Const() {
    }

    public static final String PAGE_INDEX = "index.jsp";
    public static final String PAGE_BOOKS = "books.jsp";

    public static final String PATH_SHOW = "/show";
    public static final String PATH_ADD = "/add";
    public static final String PATH_UPDATE = "/update";

    public static final String ATTR_PAGE = "page";
    public static final String ATTR_BOOK = "book";
    public static final String ATTR_ERROR_MSG = "errorMessage";
    public static final String ATTR_EDIT_BOOK = "editBook";
}
