package by.epamlab.beans;

public class Book {
    public static final String ID = "books.id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String AUTHOR_ID = "author_id";

    private final Long id;
    private final String title;
    private final Author author;
    private String description;

    public Book(String title, String description, Author author, Long id) {
        if ((title == null) || (author == null)) {
            throw new IllegalArgumentException("null field");
        }
        this.title = title;
        this.author = author;
        this.setDescription(description);
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException("null description");
        }
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Book)) {
            return false;
        }

        Book other = (Book) o;
        return (title.equals(other.title))
                && (description.equals(other.description)
                && author.equals(other.author));
    }

    @Override
    public int hashCode() {
        final int prime = 410;
        int hash = 1;
        hash = prime * hash + title.hashCode();
        hash = prime * hash + description.hashCode();
        hash = prime * hash + author.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return title + " (" + author + ")\n" + description;
    }
}
