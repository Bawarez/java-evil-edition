package by.epamlab.beans;

public class Author {
    public static final String ID = "authors.id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";

    private final Long id;
    private final String firstName;
    private final String lastName;

    public Author(String firstName, String lastName, Long id) {
        if ((firstName == null) || (lastName == null)) {
            throw new IllegalArgumentException("null field");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Author)) {
            return false;
        }

        Author other = (Author) o;
        return (firstName.equals(other.firstName))
                && (lastName.equals(other.lastName));
    }

    @Override
    public int hashCode() {
        final int prime = 410;
        int hash = 1;
        hash = prime * hash + firstName.hashCode();
        hash = prime * hash + lastName.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
