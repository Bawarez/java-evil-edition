package by.epamlab.factories;

import by.epamlab.beans.Author;
import by.epamlab.beans.Book;

import javax.servlet.ServletRequest;

/**
 * Provides Book instances
 */
public final class BookFactory {
    private BookFactory() { }

    /**
     * Provides a Book instance by given ServletRequest parameters
     * @param req ServletRequest instance with parameters for book creating
     * @return created Book instance
     */
    public static Book createBook(final ServletRequest req) {
        Long bookId = parseID(req.getParameter(Book.ID));
        Long authorId = parseID(req.getParameter(Author.ID));
        String title = req.getParameter(Book.TITLE);
        String description = req.getParameter(Book.DESCRIPTION);
        String firstName = req.getParameter(Author.FIRST_NAME);
        String lastName = req.getParameter(Author.LAST_NAME);

        Author author = new Author(firstName, lastName, authorId);
        return new Book(title, description, author, bookId);
    }

    private static Long parseID(final String id) {
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
