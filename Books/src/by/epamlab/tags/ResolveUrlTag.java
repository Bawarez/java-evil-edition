package by.epamlab.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;


public class ResolveUrlTag extends SimpleTagSupport {
    private static final String ATTR_URL = "url";
    private static final String ATTR_RESOURCE = "resource";
    private String url;

    @Override
    public void doTag() throws IOException, JspException {
        PageContext pageContext = (PageContext) getJspContext();
        URL resource = pageContext.getServletContext().getResource(url);

        pageContext.setAttribute(ATTR_URL, url);
        pageContext.setAttribute(ATTR_RESOURCE, resource);

        StringWriter stringWriter = new StringWriter();
        getJspBody().invoke(stringWriter);
        pageContext.getOut().println(stringWriter);
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
