package by.epamlab.tags;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class ListMappingTag extends TagSupport {
    private static final String ATTR_SERVLET = "servlet";
    private static final String ATTR_URL = "url";
    private Queue<? extends ServletRegistration> servlets;

    @Override
    public int doStartTag() {
        Collection<? extends ServletRegistration> values = pageContext.getServletContext()
                .getServletRegistrations()
                .values();
        servlets = new LinkedList<>(values);

        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() {
        if (!servlets.isEmpty()) {
            ServletRegistration next = servlets.poll();
            pageContext.setAttribute(ATTR_SERVLET, next.getName());
            pageContext.setAttribute(ATTR_URL, next.getMappings());
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }
}
