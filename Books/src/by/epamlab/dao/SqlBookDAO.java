package by.epamlab.dao;

import by.epamlab.beans.Author;
import by.epamlab.beans.Book;
import by.epamlab.exceptions.DAOException;
import by.epamlab.service.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides methods to work with SQL database storing book entities
 */
public final class SqlBookDAO implements BookDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlBookDAO.class);
    private static final SqlBookDAO INSTANCE = new SqlBookDAO();
    private static final SqlAuthorDAO authorDAO = SqlAuthorDAO.getInstance();
    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();
    private static final String SQL_DELETE = "delete from books where id = ?";

    private SqlBookDAO() { }

    @Override
    public List<Book> getBooks() {
        try (Connection connect = CONNECTION_POOL.getConnection();
             PreparedStatement pst = connect.prepareStatement(SelectQuery.ALL);
             ResultSet rs = pst.executeQuery()) {
            List<Book> books = new ArrayList<>();

            while (rs.next()) {
                books.add(createBook(rs));
            }
            return books;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new DAOException(e);
        }
    }

    @Override
    public Book getBook(final Long id) {
        try (Connection connect = CONNECTION_POOL.getConnection();
                PreparedStatement pst = connect.prepareStatement(SelectQuery.SINGLE)) {
            pst.setLong(SelectQuery.IND_BOOK_ID, id);
            try (ResultSet rs = pst.executeQuery()) {
                Book book = null;

                if (rs.next()) {
                    book = createBook(rs);
                }
                return book;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new DAOException(e);
        }
    }

    @Override
    public boolean saveBook(final Book book) {
        if (!areRequiredParametersNotEmpty(book)) {
            return false;
        }
        boolean isUpdate = (book.getId() != null);
        String query = (isUpdate) ? UpdateQuery.UPDATE
                                  : UpdateQuery.INSERT;
        try {
            return writeToDB(query, book, isUpdate);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteBook(Long id) {
        try (Connection connect = CONNECTION_POOL.getConnection();
             PreparedStatement pst = connect.prepareStatement(SQL_DELETE)) {
            try {
                connect.setAutoCommit(false);
                Author author = getBook(id).getAuthor();
                pst.setLong(1, id);

                boolean deleted = (pst.executeUpdate() > 0);
                if (deleted) {
                    deleteAuthorIfUnused(author);
                }
                connect.commit();

                return deleted;
            } finally {
                connect.setAutoCommit(true);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    private boolean  writeToDB(final String query, final Book book, final boolean isUpdate)
            throws SQLException {
        try (Connection connect = CONNECTION_POOL.getConnection();
             PreparedStatement pst = connect.prepareStatement(query)) {
            try {
                int rowsAffected;
                Author author = book.getAuthor();

                connect.setAutoCommit(false);
                Long actualAuthorId = authorDAO.getAuthorId(author.getFirstName(), author.getLastName());

                if (actualAuthorId == null) {
                    actualAuthorId = authorDAO.saveAuthor(author);
                }
                pst.setString(UpdateQuery.IND_TITLE, book.getTitle());
                pst.setString(UpdateQuery.IND_DESCRIPTION, book.getDescription());
                pst.setLong(UpdateQuery.IND_AUTHOR_ID, actualAuthorId);
                if (isUpdate) {
                    pst.setLong(UpdateQuery.IND_BOOK_ID, book.getId());
                }
                rowsAffected = pst.executeUpdate();

                connect.commit();

                if (isUpdate && !actualAuthorId.equals(author.getId())) {
                    deleteAuthorIfUnused(author);
                }

                return (rowsAffected > 0);
            } catch (SQLException e) {
                connect.rollback();
                connect.setAutoCommit(true);
                throw e;
            }
        }
    }

    private void deleteAuthorIfUnused(final Author author) {
        try {
            if (getBooksNumberByAuthor(author.getId()) == 0) {
                if (!authorDAO.deleteAuthor(author.getId())) {
                    LOGGER.error("Failed to delete unused author with id=" + author.getId());
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private int getBooksNumberByAuthor(final Long authorId) throws SQLException {
        try (Connection connect = CONNECTION_POOL.getConnection();
            PreparedStatement pst =
                    connect.prepareStatement(SelectQuery.COUNT_BY_AUTHOR)) {
            pst.setLong(SelectQuery.IND_AUTHOR_ID, authorId);
            try (ResultSet rs = pst.executeQuery()) {
                int count;
                if (rs.next()) {
                    count = rs.getInt(1);
                } else {
                    LOGGER.error("Failed to get number of books with author_id=" + authorId);
                    count = -1;
                }
                return count;
            }
        }
    }

    private boolean areRequiredParametersNotEmpty(final Book book) {
        Author author = book.getAuthor();
        String[] parameters = {
                book.getTitle(),
                book.getDescription(),
                author.getFirstName(),
                author.getLastName()
        };
        for (String param : parameters) {
            if (param.trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Instance accessor
     * @return an instance of SqlBookDAO
     */
    public static SqlBookDAO getInstance() {
        return INSTANCE;
    }

    private Book createBook(final ResultSet rs) throws SQLException {
        Long bookId = rs.getLong(Book.ID);
        String title = rs.getString(Book.TITLE);
        String description = rs.getString(Book.DESCRIPTION);

        return new Book(title, description, authorDAO.createAuthor(rs), bookId);
    }

    private static class UpdateQuery {
        static final String BOOKS_SET_FIELDS =
                "books set " + Book.TITLE + "=?, " + Book.DESCRIPTION + "=?, " + Book.AUTHOR_ID + "=?";
        static final String INSERT = "insert into " + BOOKS_SET_FIELDS;
        static final String UPDATE = "update " + BOOKS_SET_FIELDS + " where id=?";

        static final int IND_BOOK_ID = 4;
        static final int IND_TITLE = 1;
        static final int IND_DESCRIPTION = 2;
        static final int IND_AUTHOR_ID = 3;
    }

    private static class SelectQuery {
        static final String ALL = "select * from books inner join authors on " + Book.AUTHOR_ID + "=" + Author.ID;
        static final String SINGLE = ALL + " where " + Book.ID + "=?";
        static final String COUNT_BY_AUTHOR = "select count(*) from books where author_id=?";

        static final int IND_BOOK_ID = 1;
        static final int IND_AUTHOR_ID = 1;
    }
}
