package by.epamlab.dao;

import by.epamlab.beans.Book;

import java.util.List;

/**
 * Declares methods to work with a data storage describing books
 */
public interface BookDAO {
    /**
     * Gets all books contained in a storage
     * @return list of books contained in a source
     */
    List<Book> getBooks();

    /**
     * Gets a book by its id
     * @param id book id
     * @return an extracted book entity, or {@code null} if book with specified id was not found
     */
    Book getBook(Long id);

    /**
     * Saves book entity in a storage
     * @param book entity to be saved
     * @return operation success flag
     */
    boolean saveBook(Book book);

    /**
     * Removes the book with specified id from storage
     * @param id book id
     * @return true if the operation was successfully completed
     */
    boolean deleteBook(Long id);
}
