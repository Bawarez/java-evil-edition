package by.epamlab.dao;

import by.epamlab.beans.Author;
import by.epamlab.exceptions.DAOException;
import by.epamlab.service.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

final class SqlAuthorDAO {
    private static final SqlAuthorDAO INSTANCE = new SqlAuthorDAO();
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlAuthorDAO.class);
    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();
    private static final String INS_AUTHOR = "insert into authors set first_name=?, last_name=?";
    private static final String DEL_AUTHOR = "delete from authors where id=?";
    private static final String SELECT_AUTHOR_ID =
            "select id from authors where " + Author.FIRST_NAME + "=? and " + Author.LAST_NAME + "=?";
    private static final int IND_AUTHOR_ID = 1;
    private static final int IND_F_NAME = 1;
    private static final int IND_L_NAME = 2;

    private SqlAuthorDAO() { }

    Long saveAuthor(final Author author) throws SQLException {
        try (Connection connect = CONNECTION_POOL.getConnection();
             PreparedStatement pst =
                     connect.prepareStatement(INS_AUTHOR, Statement.RETURN_GENERATED_KEYS)) {
            pst.setString(IND_F_NAME, author.getFirstName());
            pst.setString(IND_L_NAME, author.getLastName());
            pst.execute();
            try (ResultSet generatedKeys = pst.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                } else {
                    String msg = "Failed to add author " + author;
                    LOGGER.error(msg);
                    throw new DAOException(msg);
                }
            }
        }
    }

    boolean deleteAuthor(final Long authorId) throws SQLException {
        try (Connection connect = CONNECTION_POOL.getConnection();
                    PreparedStatement pst = connect.prepareStatement(DEL_AUTHOR)) {
            pst.setLong(IND_AUTHOR_ID, authorId);
            return  (pst.executeUpdate() > 0);
        }
    }

    Long getAuthorId(final String firstName, final String lastName) throws SQLException {
        try (Connection connect = CONNECTION_POOL.getConnection();
             PreparedStatement pst = connect.prepareStatement(SELECT_AUTHOR_ID)) {
            pst.setString(IND_F_NAME, firstName);
            pst.setString(IND_L_NAME, lastName);

            try (ResultSet rs = pst.executeQuery()) {
                Long id = null;
                if (rs.next()) {
                    id = rs.getLong(1);
                }
                return id;
            }
        }
    }

    Author createAuthor(final ResultSet rs) throws SQLException {
        Long authorId = rs.getLong(Author.ID);
        String firstName = rs.getString(Author.FIRST_NAME);
        String lastName = rs.getString(Author.LAST_NAME);

        return new Author(firstName, lastName, authorId);
    }

    static SqlAuthorDAO getInstance() {
        return INSTANCE;
    }
}
