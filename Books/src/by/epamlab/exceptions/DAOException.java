package by.epamlab.exceptions;

public class DAOException extends RuntimeException {
    public DAOException(Throwable throwable) {
        super(throwable);
    }

    public DAOException(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        return getCause().getMessage();
    }
}
