package by.epamlab.exceptions;

public class SourceException extends RuntimeException {
    public SourceException(String message) {
        super(message);
    }
}
