<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/jstl/core" prefix="c" %>
<c:choose>
    <c:when test="${empty requestScope.editBook}">
        <form action="/add" method="post" id="addForm" class="addForm">
    </c:when>
    <c:otherwise>
        <form action="/update" method="post" id="addForm" class="addForm visible">
    </c:otherwise>
</c:choose>
    <div>
        <label for="title">Title: </label>
        <input type="text" value="${requestScope.book.title}" name="title" id="title" required>

        <label for="description">Description: </label>
        <textarea name="description" id="description" required>${requestScope.book.description}</textarea>
    </div>

    <div>
        <span>Author:</span><br>
        <label for="first_name">First name: </label>
        <input type="text" name="first_name" value="${requestScope.book.author.firstName}" id="first_name" required>

        <label for="last_name">Last name: </label>
        <input type="text" name="last_name" value="${requestScope.book.author.lastName}" id="last_name" required>

        <input type="hidden" name="books.id" value="${requestScope.book.id}" id="bookId">
        <input type="hidden" name="authors.id" value="${requestScope.book.author.id}">

    </div>
    <input type="submit" value="Submit">
</form>