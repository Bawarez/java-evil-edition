<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/jstl/core" prefix="c" %>
<c:forEach var="book" items="${requestScope.bookList}">
    <div class="book">
        <a href="/showBook?books.id=${book.id}">
            <div class="bookTitle">
                <c:out value="${book.title} "/>
            </div>
            <div class="author">
                <c:out value="${book.author}"/>
            </div>
            <div class="bookDescription">
                <c:out value="${book.description}"/>
            </div>
        </a>
        <div>
            <a href="/setCover?id=${book.id}">Set cover</a>
        </div>
        <c:if test="${management}">
            <form action="/bookManagement" method="POST">
                <input type="hidden" name="id" value="${book.id}">
                <input type="submit" value="delete">
            </form>
        </c:if>
    </div>
</c:forEach>