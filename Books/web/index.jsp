<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/jstl/core" prefix="c" %>
<%@ taglib uri="utilities" prefix="util" %>
<!DOCTYPE html>
<html>
<head>
    <title>Books</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css?v=0.91">
	<script src="/js/functions.js?v=0.4"></script>
</head>
<body>
	<c:import url="menu.jsp" />
	<div class="filler"></div>
	<div class="pageContent">
		<c:import url="addForm.jsp" />
		<c:if test="${not empty requestScope.errorMessage}">
			<div class="error">
				<c:out value="${requestScope.errorMessage}"/>
			</div>
		</c:if>

		<c:if test="${not empty requestScope.page}">
			<c:import url="${requestScope.page}"/>
		</c:if>

		<util:listmapping>
			${url} - ${servlet} <br>
		</util:listmapping>
		<util:resolveurl url="/IMG_4212.JPG">
			${url} - ${resource}
		</util:resolveurl>
	</div>
	<div class="fillerBottom"></div>
	<footer>
		Here can be your advertisement.
	</footer>
</body>
</html>

