function showAddForm() {
    var inputs = addForm.getElementsByTagName('input');
    var style = addForm.style;

    if (document.getElementById('bookId').value != '') {
        for (var i = inputs.length - 1; i >= 0; i--) {
            if (inputs[i].type != 'submit') {
                inputs[i].value = '';
            }
        }
        addForm.description.value = '';
    }

    if (style.display == 'block') {
        style.display = 'none';
    } else {
        style.display = 'block';
    }
}